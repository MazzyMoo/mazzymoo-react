import React from "react";
import { Grid } from "@material-ui/core/";

import InstagramIcon from "@material-ui/icons/Instagram";
import TwitterIcon from "@material-ui/icons/Twitter";
import YouTubeIcon from "@material-ui/icons/YouTube";
import classes from "./SocialMedia.module.css";

const SocialMedia = (props) => {
  return (
    <div>
      <br />
      <Grid container justify="center">
        <Grid className={classes.Twitter}>
          <a
            href="https://twitter.com"
            target="_blank"
            rel="noopener noreferrer"
          >
            <TwitterIcon style={{ fontSize: props.size }} />
          </a>
        </Grid>
        <Grid item xs={3} className={classes.Instagram}>
          <a
            href="https://www.instagram.com/mazmozdy"
            target="_blank"
            rel="noopener noreferrer"
          >
            <InstagramIcon style={{ fontSize: props.size }} />
          </a>
        </Grid>
        <Grid className={classes.YouTube}>
          <a
            href="https://www.youtube.com/channel/UCdJm_FTVSU9FMZntmDWdCmw"
            target="_blank"
            rel="noopener noreferrer"
          >
            <YouTubeIcon style={{ fontSize: props.size }} />
          </a>
        </Grid>
      </Grid>
    </div>
  );
};
export default SocialMedia;
