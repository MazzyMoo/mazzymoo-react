import React from "react";
import { Button, makeStyles, Grid } from "@material-ui/core/";

const styles = makeStyles({
  Button: {
    color: "rgb(36, 85, 74,0.863)",
    fontWeight: "bold",
    display: "inline",
    fontSize: "1.2rem",
    margin: "0 0px",
  },
});

const NavItems = () => {
  const classes = styles();
  return (
    <React.Fragment>
      <br />
      <Grid container justify="space-around">
        <Grid item xs>
          <Button href="/blog" className={classes.Button}>
            Blog
          </Button>
        </Grid>
        <Grid item>
          <Button href="/playground" className={classes.Button}>
            Playground
          </Button>
        </Grid>
        <Grid item xs>
          <Button href="/media" className={classes.Button}>
            Media
          </Button>
        </Grid>
      </Grid>
    </React.Fragment>
  );
};

export default NavItems;
