import React from "react";
import PropTypes from "prop-types";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import { makeStyles } from "@material-ui/core/";
import CssBaseline from "@material-ui/core/CssBaseline";
import useScrollTrigger from "@material-ui/core/useScrollTrigger";
import Slide from "@material-ui/core/Slide";
import Logo from "./Logo/Logo";
import NavItems from "./NavItems/NavItems";
import SocialMedia from "./Socials/SocialMedia";

const styles = makeStyles({
  AppBar: {
    height: "auto",
    color: "#555B6E",
    backgroundColor: "#FAF9F9",
    paddingBottom: "10px",
    boxShadow: "none",
    borderBottom: "1px solid #89B0AE",
    opacity: "0.95",
  },
  Toolbar: {
    display: "block",
  },
});

function HideOnScroll(props) {
  const { children, window } = props;
  // Note that you normally won't need to set the window ref as useScrollTrigger
  // will default to window.
  // This is only being set here because the demo is in an iframe.
  const trigger = useScrollTrigger({ target: window ? window() : undefined });

  return (
    <Slide appear={false} direction="down" in={!trigger}>
      {children}
    </Slide>
  );
}

HideOnScroll.propTypes = {
  children: PropTypes.element.isRequired,
  /**
   * Injected by the documentation to work in an iframe.
   * You won't need it on your project.
   */
  window: PropTypes.func,
};

const HideAppBar = (props) => {
  const classes = styles();
  return (
    <React.Fragment>
      <CssBaseline />
      <HideOnScroll {...props}>
        <AppBar position="sticky" className={classes.AppBar}>
          <Toolbar className={classes.Toolbar}>
            <Logo />
            <NavItems />
            <SocialMedia />
          </Toolbar>
        </AppBar>
      </HideOnScroll>
      {/*<Toolbar />*/}
    </React.Fragment>
  );
};
export default HideAppBar;
