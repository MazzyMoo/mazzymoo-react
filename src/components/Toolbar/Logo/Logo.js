import React from "react";
import MyLogo from "../../../assests/icon/maz-icon.png";
import { Link } from "react-router-dom";
import classes from "./Logo.module.css";

const Logo = () => {
  return (
    <React.Fragment>
      <Link to="/">
        <div
          className={classes.Icon}
          style={{ backgroundImage: { MyLogo } }}
        ></div>
      </Link>
    </React.Fragment>
  );
};

export default Logo;
