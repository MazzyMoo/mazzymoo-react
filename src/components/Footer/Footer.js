import React from "react";
import { Typography, Paper, Grid } from "@material-ui/core";
import { Link } from "react-router-dom";
import classes from "./Footer.module.css";

const Footer = () => {
  return (
    <React.Fragment>
      <Paper className={classes.Footer} elevation={0}>
        <Typography variant="caption" color="textSecondary">
          © 2020 MazMozdy
        </Typography>
        <br />
        <Link to="/contact-me" className={classes.Links}>
          <Grid container justify="center" direction="row">
            <Grid>
              <Typography variant="overline">Contact me</Typography>
            </Grid>
          </Grid>
        </Link>
      </Paper>
    </React.Fragment>
  );
};
export default Footer;
