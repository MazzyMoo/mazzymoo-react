import React from "react";
import classes from "./TagBox.module.css";
import { Paper, Typography } from "@material-ui/core";

const TagBox = (props) => {
  return (
    <React.Fragment>
      <Paper variant="outlined" className={classes.TagBox}>
        <Typography variant="overline" display="block" gutterBottom>
          {props.children}
        </Typography>
      </Paper>
    </React.Fragment>
  );
};
export default TagBox;
