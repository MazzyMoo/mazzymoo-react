import React from "react";
import { Grid, Typography } from "@material-ui/core";

import classes from "./CommentSection.module.css";

const CommentSection = (props) => {
  return (
    <Grid
      container
      direction="column"
      justify="center"
      alignItems="flex-start"
      className={classes.PostHeader}
      xl
    >
      <Typography variant="h5" gutterBottom color="textSecondary">
        Comments
      </Typography>
      <Typography variant="subtitle1" gutterBottom color="textPrimary">
        Coming soon.
      </Typography>
    </Grid>
  );
};
export default CommentSection;
