import React, { Component } from "react";
import { Container, Grid, Typography } from "@material-ui/core";
import classes from "./Post.module.css";
import TagBox from "../TagBox/TagBox";
import CommentSection from "../CommentSection/CommentSection";

import BlurOnIcon from "@material-ui/icons/BlurOn";
import SampleImg from "../../assests/thumbnails/video.jpg"; //configure with real time picture

const post = {
  title: "Conversation with Barack Obama",
  subtitle: "Take aways from attending the event in Halifax, NS.",
  image: SampleImg,
  //this will have the structure of the post [1st prototype]
  p: [
    //pass an array of paragraphs to be dispalyed in the body
    "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos blanditiis tenetur unde suscipit, quam beatae rerum inventore consectetur, neque doloribus, cupiditate numquam dignissimos laborum fugiat deleniti? Eum quasi quidem quibusdam",
    "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos blanditiis tenetur unde suscipit, quam beatae rerum inventore consectetur, neque doloribus, cupiditate numquam dignissimos laborum fugiat deleniti? Eum quasi quidem quibusdam",
    "third paragraph",
    "so on, and so forth",
  ],
  tags: [
    //tags retrieved from DB [prototype]
    "Computer Science",
    "Tag#2",
    "Travel",
    "Media",
    "Algorithms",
  ],
};

class Post extends Component {
  render() {
    return (
      <React.Fragment>
        <Container>
          <Grid
            container
            direction="column"
            justify="flex-start"
            alignItems="flex-start"
            textalign="left"
            className={classes.PostHeader}
            xl
          >
            <Typography variant="h3" gutterBottom style={{ marginTop: "50px" }}>
              {post.title}
            </Typography>
            <Typography variant="h5" gutterBottom color="textSecondary">
              {post.subtitle}
            </Typography>
          </Grid>
        </Container>

        <Grid className={classes.PostImage}>
          <img src={post.image} alt="insert img alt"></img>
        </Grid>
        <Container style={{ marginTop: 50 }}>
          <Grid
            container
            direction="column"
            justify="flex-start"
            alignItems="flex-start"
            textalign="left"
            className={classes.PostBody}
          >
            {post.p.map((paragraph, key) => {
              console.log(paragraph);
              return (
                <Typography
                  variant="body1"
                  align="justify"
                  gutterBottom
                  key={key}
                >
                  {paragraph}
                </Typography>
              );
            })}
          </Grid>
          <Grid container justify="center" className={classes.Splitter}>
            <Grid item>
              <BlurOnIcon style={{ fontSize: 10 }} />
            </Grid>
            <Grid item xs={2}>
              <BlurOnIcon style={{ fontSize: 10 }} />
            </Grid>
            <Grid item>
              <BlurOnIcon style={{ fontSize: 10 }} />
            </Grid>
          </Grid>

          <Grid
            container
            direction="row"
            justify="center"
            className={classes.PostBody}
          >
            {post.tags
              ? post.tags.map((tag, i) => {
                  return (
                    <Grid key={tag} item>
                      <TagBox>{tag} </TagBox>
                    </Grid>
                  );
                })
              : null}
          </Grid>
          <Grid container justify="center" className={classes.Splitter}>
            <Grid item>
              <BlurOnIcon style={{ fontSize: 10 }} />
            </Grid>
            <Grid item xs={2}>
              <BlurOnIcon style={{ fontSize: 10 }} />
            </Grid>
            <Grid item>
              <BlurOnIcon style={{ fontSize: 10 }} />
            </Grid>
          </Grid>
          <CommentSection />
        </Container>
      </React.Fragment>
    );
  }
}
export default Post;
