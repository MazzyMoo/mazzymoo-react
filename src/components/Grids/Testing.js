import React, { Component } from "react";
import Masonry from "react-masonry-css";
import Card from "../Cards/ElegantCard";
import Icon from "../../assests/icon/maz-icon.png";
import { Typography } from "@material-ui/core";

var items = [
  { id: 1, name: "My First Item" },
  { id: 2, name: "Another item" },
  { id: 3, name: "Third Item asdw234rtwasdf" },
  { id: 4, name: "Here is the Fourth" },
  { id: 5, name: "High FiveHigh FiveHigh FiveHigh FiveHigh FiveHigh Five" },
  { id: 6, name: "High Five" },
  { id: 7, name: "asda qwdljkoq2idfjasd 12ealkds 198yraksfij9a w12keasjhd" },
  { id: 8, name: "High Five" },
  { id: 9, name: "High Five asdfw4tsdfv" },
];

// Convert array to JSX items
items = items.map(function (item) {
  console.log(item.id);
  return (
    <img
      key={item.id}
      alt="complex"
      src={Icon}
      style={{ border: "1px solid red", height: 300 / item.id }}
    />
  );

  //return <div key={item.id}>{item.name}</div>;
});

class Testing extends Component {
  render() {
    return (
      <div>
        <p>Hello world!</p>
        <Masonry
          breakpointCols={2}
          className="my-masonry-grid"
          columnClassName=""
        >
          <Typography variant="body1" gutterBottom>
            Hu
          </Typography>
          {items}
        </Masonry>
      </div>
    );
  }
}
export default Testing;
