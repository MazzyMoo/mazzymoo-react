import React from "react";
import MasonryLayout from "react-masonry-layout";

import "./MasonaryLayout.module.css";
import SampleImg from "../../assests/thumbnails/blog.jpg";
import { Button } from "@material-ui/core";

class Masonry extends React.Component {
  state = {
    perPage: 10,
    items: [
      {
        title: "emad",
        desc: "Whew, it was an awesome time",
      },
      {
        title: "maximillian",
        desc: "I like rice and vegetables. dont hate",
      },
      {
        title: "zee",
        desc: "10 reasons why i like coding",
      },
      {
        title: "ali",
        desc: "5 games you must play while listening to jimi hendrix",
      },
      {
        title: "emad",
        desc: "etc",
      },
      {
        title: "maximillian",
        desc: "etc",
      },
      {
        title: "zee",
        desc: "etc",
      },
      {
        title: "ali",
        desc: "etc",
      },
    ],
  };

  loadItems = () => {
    //use this method to load more items
    this.setState({
      items: this.state.items.concat(Array(this.state.perPage).fill()),
    });
  };

  render() {
    //console.log(this.state.items);
    return (
      <MasonryLayout
        id="masonry-layout"
        //infiniteScroll={this.loadItems}
        style={{
          marginLeft: "auto",
          position: "relative",
          margin: "auto",
        }}
        sizes={[
          { columns: 3, gutter: 20 },
          { mq: "auto", columns: 3, gutter: 30 },
          { mq: "auto", columns: 2, gutter: 20 },
        ]}
      >
        {this.state.items.map((item, index) => {
          //console.log(item);
          //console.log("i:" + index);
          let height = index % 2 === 0 ? 300 : 200;
          return (
            <div
              key={index}
              style={{
                minWidth: "130px",
                width: "210px",
                height: `${height}px`,
                display: "block",
                borderRadius: "5px",
                marginLeft: "auto",
                //                backgroundImage: `linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url(${SampleImg})`,
                backgroundPosition: "center",
                backgroundSize: "cover",
                backgroundRepeat: "no-repeat",
              }}
            >
              <Button
                variant="contained"
                color="primary"
                href={"#contained-buttons" + index} //props.link
                style={{
                  minWidth: "130px",
                  width: "210px",
                  height: `${height}px`,
                  display: "block",
                  borderRadius: "5px",
                  backgroundImage: `linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url(${SampleImg})`,
                  backgroundPosition: "center",
                  backgroundSize: "cover",
                  backgroundRepeat: "no-repeat",
                }}
              >
                {/*<Box
                  component="div"
                  my={2}
                  m={2}
                  textOverflow="ellipsis"
                  overflow="hidden"
                  style={{
                    position: "absolute",
                    bottom: "0px",
                    left: "3px",
                    textShadow: "1px 2px #404040",
                  }}
                >
                  <Typography variant="body1" gutterBottom>
                    {item.title}
                  </Typography>
                  <Typography variant="subtitle2" gutterBottom>
                    {item.desc}
                  </Typography>
                </Box>*/}{" "}
              </Button>
            </div>
          );
        })}
      </MasonryLayout>
    );
  }
}
export default Masonry;
