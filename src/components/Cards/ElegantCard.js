import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import FavoriteIcon from "@material-ui/icons/Favorite";
import ShareIcon from "@material-ui/icons/Share";

import SampleImg from "../../assests/thumbnails/video.jpg";

const useStyles = makeStyles((theme) => ({
  root: {
    // width: "90%",
    marginTop: 40,
    marginLeft: 20,
    marginRight: 20,
    height: "auto",
  },
  media: {
    height: 0,
    paddingTop: "25.25%",
    marginLeft: "20px",
    marginRight: "20px",
    marginTop: "20px",
    marginBottom: "0px",
  },
}));

const RecipeReviewCard = () => {
  const classes = useStyles();

  return (
    <Card className={classes.root}>
      <CardMedia
        className={classes.media}
        image={SampleImg}
        title="Paella dish"
      />

      <CardContent>
        <Typography variant="h5" gutterBottom style={{ fontSize: "1.4rem" }}>
          My Post Title
        </Typography>
        <Typography variant="body1" gutterBottom>
          The story of a peeled, broken, potato
        </Typography>
      </CardContent>

      <CardActions disableSpacing>
        <IconButton aria-label="add to favorites">
          <FavoriteIcon />
        </IconButton>
        <IconButton aria-label="share">
          <ShareIcon />
        </IconButton>
      </CardActions>
    </Card>
  );
};
export default RecipeReviewCard;
