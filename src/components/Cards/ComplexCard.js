import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import ButtonBase from "@material-ui/core/ButtonBase";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
//import IconButton from "@material-ui/core/IconButton";
//import FavoriteIcon from "@material-ui/icons/Favorite";
//import ShareIcon from "@material-ui/icons/Share";
import { Link } from "react-router-dom";

import SampleImg from "../../assests/thumbnails/video.jpg";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    paddingTop: theme.spacing(2),
    paddingRight: theme.spacing(2),
    paddingLeft: theme.spacing(2),
    margin: "auto",
    maxWidth: "90%",
    marginRight: "auto",
    marginLeft: "auto",
  },
  img: {
    width: "100%",
    height: "125px",
    objectFit: "cover",
  },
}));

export default function ComplexGrid() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <ButtonBase>
        <Link
          to={"post/asd"}
          className={classes.Link}
          style={{ textDecoration: "inherit" }}
        >
          <Paper className={classes.paper}>
            <img className={classes.img} alt="complex" src={SampleImg} />

            <CardContent>
              <Typography
                variant="h5"
                gutterBottom
                style={{ fontSize: "1.4rem" }}
              >
                My Post Title
              </Typography>
              <Typography variant="body1" color="textSecondary" gutterBottom>
                The story of a peeled, broken potato
              </Typography>
            </CardContent>

            <CardActions disableSpacing>
              {/*<IconButton aria-label="add to favorites">
                <FavoriteIcon />
              </IconButton>
              <IconButton aria-label="share">
                <ShareIcon />
              </IconButton>*/}
            </CardActions>
          </Paper>
        </Link>
      </ButtonBase>
    </div>
  );
}
