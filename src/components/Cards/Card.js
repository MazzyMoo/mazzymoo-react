import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import { Link } from "react-router-dom";

const useStyles = makeStyles({
  root: {
    maxWidth: 345,
  },
  CardContent: {
    height: "160px",
    overflowY: "hidden",
  },
  Link: {
    textDecoration: "inherit",
    color: "inherit",
  },
});

const ImgMediaCard = (props) => {
  const classes = useStyles();
  const item = props.content;
  //console.log(item);

  return (
    <Card className={classes.root}>
      <Link to={"post/" + item.id} className={classes.Link}>
        <CardActionArea>
          <CardMedia
            component="img"
            alt={item.imgAlt}
            height="260"
            image={item.img}
            title={item.imgTitle}
          />
          <CardContent className={classes.CardContent}>
            <Typography gutterBottom variant="h5" component="h2">
              {item.cardTitle}
            </Typography>
            <Typography variant="body2" color="textSecondary">
              {item.cardDesc}
            </Typography>
          </CardContent>
        </CardActionArea>
      </Link>

      <CardActions>
        <Button size="small" color="primary">
          Share
        </Button>
        <Button size="small" color="primary">
          Learn More
        </Button>
      </CardActions>
    </Card>
  );
};
export default ImgMediaCard;
