import React, { Component } from "react";
import { Typography, Grid } from "@material-ui/core";
import ComplexCard from "../../components/Cards/ComplexCard";

class Playground extends Component {
  render() {
    return (
      <React.Fragment>
        <Typography
          variant="h4"
          gutterBottom
          style={{ marginTop: "50px", marginBottom: "55px" }}
        >
          Coding Playground
        </Typography>
        <Grid
          container
          alignItems="stretch"
          direction="column"
          wrap="nowrap"
          spacing={3}
        >
          <Grid item xs={12} zeroMinWidth>
            <ComplexCard />
          </Grid>
          <Grid item xs={12} zeroMinWidth>
            <ComplexCard />
          </Grid>
          <Grid item xs={12} zeroMinWidth>
            <ComplexCard />
          </Grid>
        </Grid>
      </React.Fragment>
    );
  }
}
export default Playground;
