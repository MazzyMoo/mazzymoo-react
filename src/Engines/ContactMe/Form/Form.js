import React from "react";
import classes from "./Form.module.css";
import { Typography, Grid } from "@material-ui/core";
import SocialMedia from "../../../components/Toolbar/Socials/SocialMedia";
import LinkedInIcon from "@material-ui/icons/LinkedIn";

const BasicTextFields = (props) => {
  return (
    <React.Fragment>
      <Grid item xs style={{ marginBottom: 50 }}>
        <Typography
          variant="caption"
          color="textSecondary"
          align="justify"
          gutterBottom
          style={{
            fontSize: "2rem",
          }}
        >
          If you have a project in mind or you are simply interested in finding
          out more, please get in touch and let's get things moving :)
        </Typography>
      </Grid>
      <br />
      <Grid item xs>
        <Typography variant="inherit" gutterBottom className={classes.Email}>
          <a href={"mailto: " + props.children}> {props.children}</a>
        </Typography>
      </Grid>

      <Grid item xs style={{ marginTop: 40 }}>
        <Typography
          variant="caption"
          color="textSecondary"
          align="justify"
          gutterBottom
          style={{
            fontSize: "1.8rem",
          }}
        >
          Follow me
        </Typography>
      </Grid>

      <Grid className={classes.LinkedIn}>
        <a
          href="https://www.linkedin.com/in/emad-bamatraf-532977190/"
          target="_blank"
          rel="noopener noreferrer"
        >
          <LinkedInIcon style={{ fontSize: 90 }} />
        </a>
      </Grid>
      <Grid item>
        <SocialMedia size={90} />
      </Grid>
    </React.Fragment>
  );
};
export default BasicTextFields;
