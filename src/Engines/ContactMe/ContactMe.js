import React, { Component } from "react";
import { Grid, Container, Typography } from "@material-ui/core";
import Form from "./Form/Form";

class ContactMe extends Component {
  render() {
    return (
      <Container>
        <Typography
          variant="h3"
          gutterBottom
          style={{ marginTop: "50px", marginBottom: "50px" }}
        >
          Let's talk
        </Typography>
        <Grid container direction="row" justify="center" alignItems="center">
          <Grid item>
            <Form>MazzyMozdy@gmail.com</Form>
          </Grid>
        </Grid>
      </Container>
    );
  }
}

export default ContactMe;
