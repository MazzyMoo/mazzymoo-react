import React, { Component } from "react";
import Card from "../../components/Cards/Card";
import { Container, Grid, Typography } from "@material-ui/core";

import BlogImg from "../../assests/thumbnails/blog.jpg";

class Blog extends Component {
  //fetch posts from database
  state = {
    list: null,
    posts: [
      {
        id: "post1",
        array: [{ asd: "asd" }, { asd: "dsa" }],
        tests: {
          nested1: "1",
          nested: "2",
        },
        //link: "/blog/id", //change to dynamic links and content [onclick]
        img: BlogImg,
        imgTitle: "Image Title",
        imgAlt: "Image alt tag",
        cardTitle: "Post1",
        cardDesc:
          "Lizards are a widespread group of squamate reptiles, with over 6,000 species, ranging across all continents except Antarctica",
      },
      {
        id: "post2",
        array: [{ asd: "asd" }, { asd: "dsa" }],
        tests: {
          nested1: "1",
          nested: "2",
        },
        //link: "/blog/id", //change to dynamic links and content [onclick]
        img: BlogImg,
        imgTitle: "Image Title",
        imgAlt: "Image alt tag",
        cardTitle: "Post2",
        cardDesc:
          "Lizards are a widespread group of squamate reptiles, with over 6,000 species, ranging across all continents except Antarctica",
      },
    ],
  };

  render() {
    return (
      <Container>
        <Typography
          variant="h4"
          gutterBottom
          style={{ marginTop: "50px", marginBottom: "55px" }}
        >
          Blog Posts
        </Typography>
        <Grid
          container
          direction="row"
          justify="space-evenly"
          alignItems="center"
          spacing={3}
        >
          {this.state.posts.map((post, key) => {
            //console.log(post);
            return (
              <Grid item key={key}>
                <Card content={post} key={key} />
              </Grid>
            );
          })}
        </Grid>
      </Container>
    );
  }
}

export default Blog;
