import React, { Component } from "react";
import { Container, Grid, Typography } from "@material-ui/core";
import Card from "../../components/Cards/Card";
import BlogImg from "../../assests/thumbnails/blog.jpg";
import VideoImg from "../../assests/thumbnails/video.jpg";

class Landing extends Component {
  state = {
    blog: {
      link: "/blog/id", //change to dynamic links and content
      img: BlogImg,
      imgTitle: "Image Title",
      imgAlt: "Image alt tag",
      cardTitle: "Blog Title",
      cardDesc:
        "Lizards are a widespread group of squamate reptiles, with over 6,000 species, ranging across all continents except Antarctica",
    },
    video: {
      link: "/video/id",
      img: VideoImg,
      imgTitle: "Image Title",
      imgAlt: "Image alt tag",
      cardTitle: "Video Preview",
      cardDesc:
        "Vivamus sed dui dolor. Aliquam commodo dui at purus ultricies, vel mollis urna condimentum. Aliquam rutrum quis risus sit amet euismod. Sed commodo in mi et porttitor. Duis quis mi ut elit venenatis placerat. Nulla dictum lacus ut molestie ultricies. Morbi elementum lectus in nibh fermentum, quis varius massa porttitor. Vestibulum nulla tellus, pulvinar tristique elit nec, luctus pharetra sapien. Mauris sagittis, ligula non porttitor tincidunt, urna tortor pretium mauris, a pulvinar quam orci eu augue. Proin scelerisque ante vitae malesuada porttitor.        ",
    },
  };
  render() {
    return (
      <Container>
        <Typography
          variant="h4"
          gutterBottom
          style={{ marginTop: "50px", marginBottom: "55px" }}
        >
          Some kind of welcoming message
        </Typography>
        <Grid
          container
          direction="row"
          justify="space-evenly"
          alignItems="center"
          spacing={3}
        >
          <Grid item>
            <Card content={this.state.blog} />
          </Grid>
          <Grid item>
            <Card content={this.state.video} />
          </Grid>
        </Grid>
      </Container>
    );
  }
}
export default Landing;
