import React, { Component } from "react";
import { Typography, Container, Grid } from "@material-ui/core";

import MasonaryLayout from "../../components/Grids/MasonaryLayout";

class Media extends Component {
  render() {
    return (
      <Container>
        <Typography
          variant="h4"
          gutterBottom
          style={{ marginTop: "50px", marginBottom: "55px" }}
        >
          Media
        </Typography>
        <Grid style={{ overflowX: "auto" }}>
          <MasonaryLayout />
        </Grid>
      </Container>
    );
  }
}
export default Media;
