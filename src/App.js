import React from "react";
import { Route, BrowserRouter } from "react-router-dom";
import "./App.css";

import Toolbar from "./components/Toolbar/Toolbar";
import Landing from "./Engines/Landing/Landing";
import Blog from "./Engines/Blog/Blog";
import Playground from "./Engines/Playground/Playground";
import Media from "./Engines/Media/Media";
import ContactMe from "./Engines/ContactMe/ContactMe";
import Post from "./components/Post/Post";
import Footer from "./components/Footer/Footer";

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <Toolbar />
        <Route path="/" exact component={Landing} />
        <Route path="/blog" exact component={Blog} />
        <Route path="/playground" exact component={Playground} />
        <Route path="/media" exact component={Media} />
        <Route path="/contact-me" exact component={ContactMe} />
        <Route path="/post/" exact component={Post} />
        <Footer />
      </div>
    </BrowserRouter>
  );
}

export default App;
